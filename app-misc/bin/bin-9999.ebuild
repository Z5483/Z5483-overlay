# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3

DESCRIPTION="Useful scripts written in POSIX sh"
HOMEPAGE="https://github.com/Z5483/bin"
EGIT_REPO_URI="https://github.com/Z5483/bin.git"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"

src_install() {
	rm LICENSE README || die
	dobin *
}
